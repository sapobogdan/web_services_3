package ro.bogdan.todowebservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoWebservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoWebservicesApplication.class, args);
	}

}
