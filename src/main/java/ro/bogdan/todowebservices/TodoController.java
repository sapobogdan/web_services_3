package ro.bogdan.todowebservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TodoController {

    @Autowired
    private TodoDao todoDao;

    @PostMapping("/todos")
    @ResponseBody
    public Todo addNew(@RequestBody Todo todo) {
        todoDao.save(todo);
        return todo;
    }

    @GetMapping("/todos")
    @ResponseBody
    public Iterable<Todo> getAll() {
        return todoDao.findAll();
    }

    @GetMapping("/todos/{id}")
    @ResponseBody
    public Todo getOne(@PathVariable("id") Integer todoId) {
        return todoDao.findById(todoId).get();
    }

    @PutMapping("/todos/{id}")
    @ResponseBody
    public Todo update(@RequestBody Todo updatedTodo, @PathVariable("id") Integer todoId) {
        Todo existingTodoInDatabase = todoDao.findById(todoId).get();

        existingTodoInDatabase.setDescription(updatedTodo.getDescription());
        existingTodoInDatabase.setSolved(updatedTodo.isSolved());

        todoDao.save(existingTodoInDatabase);

        return existingTodoInDatabase;
    }

    @DeleteMapping("/todos/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Integer todoId) {
        todoDao.deleteById(todoId);
        return "ok";
    }

}
